package com.mobileread.ixtab.collman;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.security.SecureStorage;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KMenuPeer;
import com.amazon.kindle.restricted.device.impl.ChromeImplementation;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.actions.DeleteAllCollectionsAction;
import com.mobileread.ixtab.collman.actions.FileSystemImporterAction;
import com.mobileread.ixtab.collman.actions.LegacyExporterAction;
import com.mobileread.ixtab.collman.actions.LegacyImporterAction;
import com.mobileread.ixtab.collman.actions.ToggleFilterLowerPanelOnlyAction;
import com.mobileread.ixtab.collman.actions.ToggleShowCollectionsOnlyAction;
import com.mobileread.ixtab.collman.actions.ToggleShowInvisibleItemsAction;
import com.mobileread.ixtab.collman.actions.ToggleShowUnfiledItemsOnlyAction;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.ui.ActionsPanel;
import com.mobileread.ixtab.collman.ui.Borders;
import com.mobileread.ixtab.collman.ui.BrowsePanel;

public abstract class CollectionManager extends JPanel {

	private static final long serialVersionUID = 1L;

	protected final KindletContext context;
	protected BookletContext bookletContext;
	protected ChromeImplementation chromeImplementation;

	public CollectionManager(KindletContext context) {
		this.context = context;
		bookletContext = getBookletContext();
		chromeImplementation = getChromeImplementation();
		Settings.init((SecureStorage) context.getService(SecureStorage.class));
		addMenu();
		modifyToolbar();
		Catalog.init(isSearchSupported());
		Clipboard.init();
	}

	protected abstract boolean isSearchSupported();

	public void init() {
		JPanel panel = new JPanel();
		this.setLayout(new BorderLayout());
		this.add(panel, BorderLayout.CENTER);
		panel.setBorder(Borders.WHITE5);
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		panel.add(Status.label, gbc);
		BrowsePanel upper = new BrowsePanel(PanelPosition.UPPER);

		BrowsePanel lower = new BrowsePanel(PanelPosition.LOWER);

		panel.add(upper, gbc);
		panel.add(new ActionsPanel(upper, lower), gbc);
		panel.add(lower, gbc);

		// simple way to bring buttons into right state
		if (Settings.get().isHideLowerPanel()) {
			Event.post(Event.LOWER_PANEL_DISABLE, this, PanelPosition.BOTH);
		}
		Event.post(Event.STARTUP, this, PanelPosition.BOTH);

		Container root = context.getRootContainer();
		root.setLayout(new BorderLayout());
		root.add(this, BorderLayout.CENTER);
		root.validate();
	}

	protected abstract BookletContext getBookletContext();
	
	private ChromeImplementation getChromeImplementation() {
		return (ChromeImplementation) Framework
				.getService(com.amazon.kindle.booklet.ChromeHeaderBar.class);
	}

	protected abstract void modifyToolbar();
	
	private void addMenu() {
		KMenu menu = new KMenu();
		int defaults = menu.getItemCount();

		menu.add(new KMenuItem(new ShowInvisibleInHomeButton()));
		menu.add(new KMenuItem(new ShowCollectionsOnlyButton()));
		menu.add(new KMenuItem(new FilterLowerPanelOnlyButton()));
		menu.add(new KMenuItem(new ShowUnfiledItemsOnlyButton()));

		menu.add(new KMenuItem(new LegacyImporterAction()));
		menu.add(new KMenuItem(new LegacyExporterAction()));
		menu.add(new KMenuItem(new FileSystemImporterAction()));
		menu.add(new KMenuItem(new DeleteAllCollectionsAction()));
		
		
		removeDefaultItems(menu, defaults);
		context.setMenu(menu);
	}

	private static class ShowInvisibleInHomeButton extends JCheckBox implements ActionListener {
		private static final long serialVersionUID = 1L;

		public ShowInvisibleInHomeButton() {
			super(new ToggleShowInvisibleItemsAction());
			setSelected(!Settings.get().isHideInvisibleInHome());
			Event.addListener(this);
		}
		
		public void actionPerformed(ActionEvent event) {
			Event e = (Event) event;
			if (e.getID() == Event.INVISIBLE_SHOW) {
				setSelected(true);
			} else if (e.getID() == Event.INVISIBLE_HIDE) {
				setSelected(false);
			}
		}
	}
	
	private static class ShowCollectionsOnlyButton extends JCheckBox implements ActionListener {
		private static final long serialVersionUID = 1L;

		public ShowCollectionsOnlyButton() {
			super(new ToggleShowCollectionsOnlyAction());
			setSelected(Settings.get().isShowCollectionsOnly());
			Event.addListener(this);
		}
		
		public void actionPerformed(ActionEvent event) {
			Event e = (Event) event;
			if (e.getID() == Event.DISPLAY_ALL) {
				setSelected(false);
			} else if (e.getID() == Event.DISPLAY_COLLECTIONS_ONLY) {
				setSelected(true);
			}
		}
	}
	
	private static class FilterLowerPanelOnlyButton extends JCheckBox implements ActionListener {
		private static final long serialVersionUID = 1L;

		public FilterLowerPanelOnlyButton() {
			super(new ToggleFilterLowerPanelOnlyAction());
			setSelected(Settings.get().isFilterLowerPanelOnly());
			Event.addListener(this);
		}
		
		public void actionPerformed(ActionEvent event) {
			Event e = (Event) event;
			if (e.getID() == Event.FILTER_LOWER_ONLY_OFF) {
				setSelected(false);
			} else if (e.getID() == Event.FILTER_LOWER_ONLY_ON) {
				setSelected(true);
			}
		}
	}
	
	private static class ShowUnfiledItemsOnlyButton extends JCheckBox implements ActionListener {
		private static final long serialVersionUID = 1L;

		public ShowUnfiledItemsOnlyButton() {
			super(new ToggleShowUnfiledItemsOnlyAction());
			setSelected(Settings.get().isShowUnfiledItemsOnly());
			Event.addListener(this);
		}
		
		public void actionPerformed(ActionEvent event) {
			Event e = (Event) event;
			if (e.getID() == Event.SHOW_UNFILED_ONLY_OFF) {
				setSelected(false);
			} else if (e.getID() == Event.SHOW_UNFILED_ONLY_ON) {
				setSelected(true);
			}
		}
	}
	
	private void removeDefaultItems(KMenu menu, int count) {
		try {
			Field f = menu.getClass().getDeclaredField("I");
			f.setAccessible(true);
			KMenuPeer peer = (KMenuPeer) f.get(menu);
			for (int i = count - 1; i >= 0; --i) {
				peer.remove(i);
			}
		} catch (Throwable t) {
		}
	}

	public abstract void onStop();
}
