package com.mobileread.ixtab.collman;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class Status {
	static final JLabel label;
	static {
		label = new JLabel("");
	}
	public static void set(String status) {
		final String display = status == null || status.trim().equals("") ? null : status.trim();
		SwingUtilities.invokeLater(new Runnable(){

			public void run() {
				if (display == null) {
					label.setVisible(false);
				} else {
					label.setVisible(true);
					label.setText(" ▶ "+ display);
				}
			}});
	}
}
