package com.mobileread.ixtab.collman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import com.amazon.kindle.kindlet.security.SecureStorage;

public class Settings implements ActionListener {

	private static final String COLLECTIONS_ONLY = "collectionsOnly";
	private static final String HIDE_INVISIBLE = "hideInvisible";
	private static final String HIDE_LOWER_PANEL = "hideLowerPanel";
	private static final String FILTER_LOWER_PANEL_ONLY = "filterLowerPanelOnly";
	private static final String SHOW_UNFILED_ONLY = "showUnfiledOnly";
	private static final byte[] FALSE = new byte[] { 0 };
	private static final byte[] TRUE = new byte[] { 1 };

	private static Settings instance;

	private final SecureStorage storage;
	private boolean persistSettings = true;

	public static void init(SecureStorage storage) {
		instance = new Settings(storage);
	}

	public static Settings get() {
		return instance;
	}

	public Settings(SecureStorage storage) {
		this.storage = storage;
		Event.addListener(this);
	}

	public boolean isShowCollectionsOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(COLLECTIONS_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isHideInvisibleInHome() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(HIDE_INVISIBLE));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isHideLowerPanel() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(HIDE_LOWER_PANEL));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isFilterLowerPanelOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(FILTER_LOWER_PANEL_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}

	public boolean isShowUnfiledItemsOnly() {
		try {
			return Arrays.equals(TRUE, storage.getBytes(SHOW_UNFILED_ONLY));
		} catch (Throwable t) {
		}
		return false;
	}

	public void actionPerformed(ActionEvent e) {
		onActionPerformed((Event) e);
	}

	private boolean onActionPerformed(Event e) {
		if (!persistSettings) {
			if (e.getID() == Event.SETTINGS_MODE_PERSISTENT) {
				persistSettings = true;
			}
			return true;
		} else {
			switch (e.getID()) {
			case Event.SETTINGS_MODE_EPHEMERAL:
				persistSettings = false;
				return true;
			case Event.DISPLAY_ALL:
				return set(COLLECTIONS_ONLY, FALSE);
			case Event.DISPLAY_COLLECTIONS_ONLY:
				return set(COLLECTIONS_ONLY, TRUE);
			case Event.INVISIBLE_HIDE:
				return set(HIDE_INVISIBLE, TRUE);
			case Event.INVISIBLE_SHOW:
				return set(HIDE_INVISIBLE, FALSE);
			case Event.LOWER_PANEL_DISABLE:
				return set(HIDE_LOWER_PANEL, TRUE);
			case Event.LOWER_PANEL_ENABLE:
				return set(HIDE_LOWER_PANEL, FALSE);
			case Event.FILTER_LOWER_ONLY_ON:
				return set(FILTER_LOWER_PANEL_ONLY, TRUE);
			case Event.FILTER_LOWER_ONLY_OFF:
				return set(FILTER_LOWER_PANEL_ONLY, FALSE);
			case Event.SHOW_UNFILED_ONLY_ON:
				return set(SHOW_UNFILED_ONLY, TRUE);
			case Event.SHOW_UNFILED_ONLY_OFF:
				return set(SHOW_UNFILED_ONLY, FALSE);
			default:
				return false;
			}
		}
	}

	private boolean set(String key, byte[] value) {
		try {
			storage.putBytes(key, value);
		} catch (Throwable t) {
			return false;
		}
		return true;
	}
}
