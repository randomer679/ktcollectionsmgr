package com.mobileread.ixtab.collman.catalog;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public abstract class Entry {

	protected Entry() {
	}

	public static String getAuthor(CatalogEntry entry) {
		try {
			String credits = CatalogAdapter.INSTANCE.getFirstCreditAsJSON(entry);
			if (credits == null) {
				return null;
			}
			JSONObject o = (JSONObject) JSONValue.parse(credits);
			o = (JSONObject) o.get("name");
			return (String) o.get("display");
		} catch (Throwable t) {
			return null;
		}
	}
	
	public static Entry instantiate(CatalogEntry catalogEntry) {
		if (catalogEntry instanceof CatalogEntryCollection) {
			return new EntryCollection((CatalogEntryCollection)catalogEntry);
		} else if (catalogEntry instanceof CatalogItem) {
			return new Item((CatalogItem)catalogEntry);
		}
		throw new RuntimeException("unsupported entry type: "+catalogEntry.getClass().getName());
	}

	public abstract CatalogEntry getBackend();
	protected abstract void setBackend(CatalogEntry update);

	protected abstract boolean isCollection();
	
	public static final RootCollection getRoot() {
		return new RootCollection();
	}
	
	public final boolean isRoot() {
		return equals(this, getRoot());
	}
	
	public String toString() {
		return " " + getName();
	}

	public Object getUUID() {
		return CatalogAdapter.INSTANCE.getUUID(getBackend());
	}
	
	public boolean isVisible() {
		return CatalogAdapter.INSTANCE.isVisibleInHome(getBackend());
	}

	public void setVisible(boolean visible) {
		MutableEntry mutable = Catalog.createMutableEntry(this);
		CatalogAdapter.INSTANCE.setIsVisibleInHome(mutable, visible);
		Catalog.update(mutable);
		reload();
	}

	public void reload() {
		if (!isRoot()) {
			setBackend(Catalog.load(getUUID()));
		}
		Event.post(Event.ENTRY_CHANGED, this, PanelPosition.BOTH);
	}

	public static boolean equals(Object o1, Object o2) {
		if (o1 == null || o2 == null || !(o1 instanceof Entry && o2 instanceof Entry)) {
			return false;
		}
		Entry e1 = (Entry)o1;
		Entry e2 = (Entry)o2;
		return (e1.getUUID().equals(e2.getUUID()));
	}

	public static void copyBackend(Object targetEntry, Object sourceEntry) {
		Entry target = (Entry)targetEntry;
		Entry source = (Entry)sourceEntry;
		target.setBackend(source.getBackend());
	}
	
	public String getName() {
		return CatalogAdapter.INSTANCE.getTitle(getBackend());
	}

	public int getCollectionsCount() {
		return CatalogAdapter.INSTANCE.getCollectionCount(getBackend());
	}
	
	public Collection[] getCollections() {
		Object[] uuids = CatalogAdapter.INSTANCE.getCollections(getBackend());
		if (uuids == null || uuids.length == 0) {
			return new Collection[0];
		}
		Collection[] parents = new Collection[uuids.length];
		for (int i=0; i < parents.length; ++i) {
			parents[i] = (Collection) Entry.instantiate(Catalog.load(uuids[i]));
		}
		return parents;
	}
	
}
