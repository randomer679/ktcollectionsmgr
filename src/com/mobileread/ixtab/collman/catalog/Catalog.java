package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;

public class Catalog {
	
	public static final Predicate ALL_ENTRIES = PredicateFactory.startsWith("titles[0].display", "");
	
	private static SearchCache cache;
	private static final Predicate filter = PredicateFactory.and(new Predicate[] {
			// do not use isFalse here, because it will filter out null values (which we want)
			PredicateFactory.not(PredicateFactory.isTrue("isArchived")),
			PredicateFactory.not(PredicateFactory.isTrue("isDownloading")),
			PredicateFactory.not(PredicateFactory.equals("mimeType", "application/x-kindle-acx")),
			
			PredicateFactory.or(new Predicate[]{
					PredicateFactory.equals("type", "Collection"),
					PredicateFactory.startsWith("type", "Entry:Item"),
			}),
			
	});
		
	private static final CatalogService backend = CatalogService.INSTANCE;
	private Catalog(){
	};
	
	public static void init(boolean isSearchSupported) {
		cache = createCache(isSearchSupported);
	}
	
	private static SearchCache createCache(boolean isSearchSupported) {
		if (isSearchSupported) {
			final WorkingSearchCache cache = new WorkingSearchCache();
			new Thread() {
	
				public void run() {
					cache.init(find(ALL_ENTRIES, true, 0, Integer.MAX_VALUE, null, QueryResultDepth.FAST));
				}
				
			}.start();
			return cache;
		} else {
			return new NoOpSearchCache();
		}
	}
	
	public static SearchCache getCache() {
		return cache;
	}
	
	public static CatalogEntry[] find(Predicate predicate) {
		CatalogEntry[] result = find(predicate, true, 0, Integer.MAX_VALUE, null);
		return result;
	}
	
	public static CatalogEntry[] find(Predicate predicate, boolean useStandardFilter, final int offset, final int maxResults, final int[] totalCountPointer) {
		return find(predicate, useStandardFilter, offset, maxResults, totalCountPointer, QueryResultDepth.FULL);
	}
	
	public static CatalogEntry[] find(Predicate predicate, boolean useStandardFilter, final int offset, final int maxResults, final int[] totalCountPointer, final QueryResultDepth depth) {
		if (useStandardFilter) {
			predicate = PredicateFactory.and(new Predicate[] { predicate,
					filter });
		}
		
		return backend.find(predicate, offset, maxResults, totalCountPointer, depth);
	}

	public static MutableEntry createMutableEntry(Entry entry) {
		return backend.createMutableEntry(entry.getUUID());
	}

	public static MutableCollection createMutableCollection(
			Collection collection) {
		return backend.createMutableCollection(collection.getUUID());
	}
	
	public static void update(MutableEntry entry) {
		CatalogTransaction t = backend.openTransaction();
		t.updateEntry(entry);
		t.commitSync();
	}
	
	public static CatalogEntry load(Object uuid) {
		return find(getPredicateForUUID(uuid))[0];
	}

	private static Predicate getPredicateForUUID(Object uuid) {
		return PredicateFactory.equals("uuid", uuid.toString());
	}

	public static void delete(Object uuid) {
		CatalogTransaction t = backend.openTransaction();
		t.deleteEntry(uuid);
		t.commitSync();
	}

	public static Collection createCollection(Collection parent,
			String newCollectionTitle) {
		Object uuid = createRandomUUID();
		MutableCollection c = backend.createMutableCollection(uuid);
		CatalogAdapter.INSTANCE.setTitle(c, newCollectionTitle);
		CatalogAdapter.INSTANCE.setIsVisibleInRoot(c, parent.isRoot());
		CatalogTransaction t = backend.openTransaction();
		t.addEntry(c);
		boolean ok = t.commitSync();
		if (ok) {
			parent.addEntry(Entry.instantiate(load(uuid)));
			getCache().set(uuid, newCollectionTitle);
		}
		return (Collection)Entry.instantiate(load(uuid));
	}

	public static CatalogService getBackend() {
		return backend;
	}

	public static Object createRandomUUID() {
		return backend.createNewUUID();
	}

	
}
