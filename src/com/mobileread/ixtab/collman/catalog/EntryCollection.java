package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;

public class EntryCollection extends Collection {

	private CatalogEntryCollection backend;
	
	public EntryCollection(CatalogEntryCollection catalogEntry) {
		this.backend = catalogEntry;
	}

	public CatalogEntry getBackend() {
		return backend;
	}

	protected boolean isCollection() {
		return true;
	}

	protected void setBackend(CatalogEntry update) {
		backend = (CatalogEntryCollection) update;
	}

}
