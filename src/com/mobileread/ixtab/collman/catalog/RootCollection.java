package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;

public class RootCollection extends Collection {

	private static final Object uuid = Catalog.createRandomUUID();
	
	public CatalogEntry getBackend() {
		return null;
	}
	
	protected void setBackend(CatalogEntry backend) {
	}

	public boolean isVisible() {
		return true;
	}
	
	public Object getUUID() {
		return uuid;
	}

	public String getName() {
		return "/";
	}

	public String toString() {
		return getName();
	}
}
