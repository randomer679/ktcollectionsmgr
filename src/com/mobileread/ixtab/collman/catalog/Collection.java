package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;

public abstract class Collection extends Entry {

	protected boolean isCollection() {
		return true;
	}

	public CatalogEntryCollection getBackendCollection() {
		return (CatalogEntryCollection) getBackend();
	}
	
	private final int[] totalSize = new int[1];
	
	public Entry[] getEntries(Predicate filter, int offset, int count) {
		Entry[] result = new Entry[count];
		Predicate pred = Catalog.ALL_ENTRIES;
		if (!isRoot()) {
			Object[] members = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			if (members == null) {
				members = new Object[0];
			}
			pred = PredicateFactory.inList("uuid", members);
		}
		if (filter != null) {
			pred = PredicateFactory.and(new Predicate[] {
					pred,
					filter,
			});
		}
		CatalogEntry[] ces = Catalog.find(pred, true, offset, count, totalSize);
		for (int i=0; i < ces.length; ++i) {
			result[i] = Entry.instantiate(ces[i]);
		}
		return result;
	}

	public boolean addEntry(Entry child) {
		if (child.getUUID().equals(getUUID())) {
			return false;
		}
		
		if (!isRoot()) {
			Object[] members = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			MutableCollection mutable = Catalog.createMutableCollection(this);
			for (int i=0; i < members.length; ++i) {
				CatalogAdapter.INSTANCE.addMember(mutable, members[i]);
				if (members[i].equals(child.getUUID())) {
					return true;
				}
			}
			CatalogAdapter.INSTANCE.addMember(mutable, child.getUUID());
			Catalog.update(mutable);
		}
		reload();
		childUpdated(child);
		return true;
	}

	public void removeEntry(Entry source) {
		if (!isRoot() && source.getUUID().equals(getUUID())) {
			return;
		}
		if (isRoot()) {
			source.setVisible(false);
		} else {
			
			Object[] members = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
			boolean removed = false;
			boolean isEmpty = true;
			MutableCollection mutable = Catalog.createMutableCollection(this);
			for (int i=0; i < members.length; ++i) {
				Object uuid = members[i];
				if (!uuid.equals(source.getUUID())) {
					CatalogAdapter.INSTANCE.addMember(mutable, uuid);
					isEmpty = false;
				} else {
					removed = true;
				}
			}
			if (!removed) {
				return;
			}
			if (isEmpty) {
				CatalogAdapter.INSTANCE.setMembers(mutable, new Object[0]);
			}
			Catalog.update(mutable);
		}
		reload();
		childUpdated(source);

	}

	private void childUpdated(Entry child) {
		child.reload();
		if (CatalogAdapter.INSTANCE.getCollectionCount(child.getBackend()) == 0 && !child.isVisible()) {
			child.setVisible(true);
		}
	}

	public void renameTo(String newTitle) {
		Catalog.getCache().set(getUUID(), newTitle);
		MutableEntry mutable = Catalog.createMutableEntry(this);
		CatalogAdapter.INSTANCE.setTitle(mutable, newTitle);
		Catalog.update(mutable);
		Object[] parents = CatalogAdapter.INSTANCE.getCollections(getBackend());
		for (int i=0; i < parents.length; ++i) {
			Entry.instantiate(Catalog.load(parents[i])).reload();
		}
		reload();
		
	}

	public boolean isEmpty() {
		return CatalogAdapter.INSTANCE.getMembers(getBackendCollection()).length == 0;
	}

	public void delete() {
		
		Object[] children = CatalogAdapter.INSTANCE.getMembers(getBackendCollection());
		Object[] parents = CatalogAdapter.INSTANCE.getCollections(getBackend());
		Catalog.getCache().delete(getUUID());
		Catalog.delete(getUUID());
		Event.post(Event.COLLECTION_DELETED, this, PanelPosition.BOTH);
		
		for (int i=0; i < parents.length; ++i) {
			Entry.instantiate(Catalog.load(parents[i])).reload();
		}
		for (int i=0; i < children.length; ++i) {
			childUpdated(Entry.instantiate(Catalog.load(children[i])));
		}
	}

	public int getSize() {
		if (isRoot()) {
			return totalSize[0];
		}
		return CatalogAdapter.INSTANCE.getMemberCount(getBackendCollection());
	}

	public CatalogEntry getBackend() {
		return null;
	}

	protected void setBackend(CatalogEntry update) {
	}

	public String toString() {
		return super.toString() + " ["+getSize()+"]";
	}

	
	
}
