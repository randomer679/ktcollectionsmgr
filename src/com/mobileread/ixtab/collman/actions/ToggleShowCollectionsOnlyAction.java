package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public class ToggleShowCollectionsOnlyAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	public ToggleShowCollectionsOnlyAction() {
		super(I18n.get().i18n(I18n.DISPLAY_COLLECTIONS_KEY, I18n.DISPLAY_COLLECTIONS_VALUE));
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(Settings.get().isShowCollectionsOnly() ? Event.DISPLAY_ALL : Event.DISPLAY_COLLECTIONS_ONLY, this, PanelPosition.BOTH);
	}
	
}
