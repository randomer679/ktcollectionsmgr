package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.home.resources.DialogResources;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.Clipboard;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Model;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;
import com.mobileread.ixtab.collman.icons.Icons;

public class CreateOrRenameCollectionAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private JButton button;
	private static final ImageIcon createIcon = Icons.load(Icons.CREATE);
	private static final ImageIcon renameIcon = Icons.load(Icons.RENAME);
	private boolean isRenaming = false;

	public CreateOrRenameCollectionAction() {
		Event.addListener(this);
	}
	
	public void setButton(JButton button) {
		button.setText("");
		this.button = button;
		button.setIcon(createIcon);
	}

	public void actionPerformed(ActionEvent e) {
		if (e instanceof Event) {
			updateState((Event)e);
			return;
		}
		performAction(e);
	}

	private void updateState(Event e) {
		if (e.getID() == Event.CLIPBOARD_CHANGED) {
			if (Clipboard.get().countItems(PanelPosition.BOTH) == 1 && Clipboard.get().getItems(PanelPosition.BOTH)[0] instanceof Collection) {
				if (!isRenaming) {
					isRenaming = true;
					setIcon(renameIcon);
				}
			} else {
				if (isRenaming) {
					isRenaming = false;
					setIcon(createIcon);
				}
			}
		}
	}

	private void setIcon(ImageIcon icon) {
		button.setIcon(icon);
	}

	private void performAction(ActionEvent e) {
		if (isRenaming) {
			Entry[] entries = Clipboard.get().getItems(PanelPosition.BOTH);
			if (entries.length != 1 || !(entries[0] instanceof Collection)) {
				return;
			}
			String currentName = entries[0].getName().trim();
			String newName = askForName(currentName);
			if (newName != null && !newName.equals(currentName)) {
				((Collection)entries[0]).renameTo(newName);
			}
		} else {
			String newName = askForName("");
			if (newName == null) {
				return;
			}
			Catalog.createCollection(Model.get(PanelPosition.UPPER).getCurrentCollection(), newName);
		}
	}

	private String askForName(String currentName) {
		String dialogTitle = ResourceBundle.getBundle(DialogResources.class.getName()).getString("addcollection.title");
		String newName = KOptionPane.showInputDialog(button, dialogTitle, currentName);
		if (newName == null || newName.trim().equals("") || isItemWithNamePresent(newName)) {
			return null;
		}
		return newName.trim();
	}

	private boolean isItemWithNamePresent(String title) {
		title = title.trim();
		// dunno why, but equals sometimes returned wrong results!???
		Predicate search = PredicateFactory.startsWith("titles[0].display", title);
		CatalogEntry[] existing = Catalog.find(search, false, 0, Integer.MAX_VALUE, null);
		for (int i=0; i < existing.length; ++i) {
			if (CatalogAdapter.INSTANCE.getTitle(existing[i]).equals(title)) {
				return true;
			}
		}
		return false;
	}

}
