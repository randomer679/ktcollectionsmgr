package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.sync.LegacyImporter;

public class LegacyImporterAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public LegacyImporterAction() {
		super(I18n.get().i18n(I18n.IMPORT_CALIBRE_KEY, I18n.IMPORT_CALIBRE_VALUE));
		setEnabled(LegacyImporter.canRun());
	}
	
	public void actionPerformed(ActionEvent e) {
		new LegacyImporter().start();
	}

}
