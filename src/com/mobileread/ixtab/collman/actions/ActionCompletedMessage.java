package com.mobileread.ixtab.collman.actions;

import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Status;

public class ActionCompletedMessage {
	private static final String message = I18n.get().i18n(I18n.ACTION_DONE_KEY, I18n.ACTION_DONE_VALUE);

	public static void show() {
		Status.set(message);
		Event.post(Event.STARTUP, ActionCompletedMessage.class, PanelPosition.BOTH);
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Status.set(null);
				//KOptionPane.showMessageDialog(null, message);
			}});
	}
}
