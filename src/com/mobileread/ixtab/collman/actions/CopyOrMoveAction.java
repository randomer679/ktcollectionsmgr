package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Clipboard;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class CopyOrMoveAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private final int sourcePosition;
	private final boolean move;
	private Collection sourceCollection = Entry.getRoot();
	private Collection targetCollection = Entry.getRoot();

	public CopyOrMoveAction(int sourcePosition, boolean move) {
		Event.addListener(this);
		this.sourcePosition = sourcePosition;
		this.move = move;
	}

	public void actionPerformed(ActionEvent e) {
		if (e instanceof Event) {
			modifyState((Event) e);
			return;
		}
		copyOrMoveSelected();
	}

	private void modifyState(Event e) {
		if (e.getSource() instanceof Collection && (e.getID() == Event.PATH_ENTRY_CLICKED || e.getID() == Event.VIEW_ENTRY_CLICKED)) {
			if (e.appliesTo(sourcePosition)) {
				sourceCollection = (Collection) e.getSource();
			} else {
				targetCollection = (Collection) e.getSource();
			}
		}
	}

	private void copyOrMoveSelected() {
		Entry[] sources = Clipboard.get().getItems(sourcePosition);
		for (int i = 0; i < sources.length; ++i) {
			copyOrMove(sources[i]);
		}
	}

	private void copyOrMove(Entry source) {
		boolean ok = targetCollection.addEntry(source);
		if (ok && move) {
			sourceCollection.removeEntry(source);
		}
	}

}
