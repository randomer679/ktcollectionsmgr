package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Clipboard;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class DeleteCollectionsAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public void actionPerformed(ActionEvent e) {
		Entry[] entries = Clipboard.get().getItems(PanelPosition.BOTH);
		for (int i=0; i < entries.length; ++i) {
			if (entries[i] instanceof Collection) {
				((Collection)entries[i]).delete();
			}
		}
		Entry.getRoot().reload();
	}
}
