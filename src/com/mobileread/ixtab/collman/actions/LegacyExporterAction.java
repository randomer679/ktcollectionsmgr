package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.sync.LegacyExporter;

public class LegacyExporterAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public LegacyExporterAction() {
		super(I18n.get().i18n(I18n.EXPORT_CALIBRE_KEY, I18n.EXPORT_CALIBRE_VALUE));
	}
	
	public void actionPerformed(ActionEvent e) {
		new LegacyExporter().start();
	}

}
