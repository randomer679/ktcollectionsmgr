package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.sync.AllCollectionsRemover;

public class DeleteAllCollectionsAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public DeleteAllCollectionsAction() {
		super(I18n.get().i18n(I18n.DELETE_ALL_MENU_KEY, I18n.DELETE_ALL_MENU_VALUE));
	}
	
	public void actionPerformed(ActionEvent e) {
		String msg = I18n.get().i18n(I18n.DELETE_ALL_MESSAGE_KEY, I18n.DELETE_ALL_MESSAGE_VALUE);
		String title = I18n.get().i18n(I18n.DELETE_ALL_MENU_KEY, I18n.DELETE_ALL_MENU_VALUE);
		if (KOptionPane.OK_OPTION == KOptionPane.showConfirmDialog(null, msg, title, KOptionPane.CANCEL_OK_OPTION)) {
			new Thread() {

				public void run() {
					AllCollectionsRemover.run();
					ActionCompletedMessage.show();
				}
			}.start();
		}
	}

}
