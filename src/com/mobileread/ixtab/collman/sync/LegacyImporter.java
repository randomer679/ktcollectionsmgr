package com.mobileread.ixtab.collman.sync;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.mobileread.ixtab.collman.Status;
import com.mobileread.ixtab.collman.actions.ActionCompletedMessage;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class LegacyImporter extends Thread {

	static final String JSON_FILE = "/mnt/us/system/collections.json";

	private static final Predicate COLLECTION_PREDICATE = PredicateFactory.equals("type", "Collection");
	
	public static boolean canRun() {
		File f = new File(JSON_FILE);
		return f.exists() && f.isFile() && f.canRead();
	}
	
	public void run() {
		Status.set("");
		JSONObject legacy = loadLegacyFile();
		if (legacy == null) {
			Status.set("Unable to load system/collections.json");
			return;
		}
		updateCollections(legacy);
		
		ActionCompletedMessage.show();
	}

	private JSONObject loadLegacyFile() {
		try {
			File f = new File(JSON_FILE);
			if (f.exists() && f.canRead()) {
				return (JSONObject) JSONValue.parse(new FileReader(f));
			}
		} catch (Throwable t) {}
		return null;
	}

	private void updateCollections(JSONObject root) {
		Iterator it = root.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (java.util.Map.Entry) it.next();
			String name = (String) entry.getKey();
			name = removeLocale(name);
			JSONObject content = (JSONObject) entry.getValue();
			updateCollection(name, content);
		}
	}

	private String removeLocale(String name) {
		int at = name.lastIndexOf('@');
		if (at != -1) {
			name = name.substring(0, at);
		}
		return name;
	}
	
	private void updateCollection(String name, JSONObject content) {
		JSONArray items = null;
		{
			Object o = content.get("items");
			if (o instanceof JSONArray) {
				items = (JSONArray) o;
			}
		}
		
		if (items == null) {
			return;
		}

		Predicate predicate = PredicateFactory.and(new Predicate[] {
				PredicateFactory.equals("titles[0].display", name),
				COLLECTION_PREDICATE,
		});
		
		int[] total = new int[1];
		CatalogEntry[] entries = Catalog.find(predicate, true, 0, 1, total, QueryResultDepth.FULL);
		Collection collection = null;
		if (total[0] > 1) {
			// should never happen
			return;
		} else if (total[0] == 0) {
			collection = Catalog.createCollection(Entry.getRoot(), name);
		} else {
			collection = (Collection) Entry.instantiate(entries[0]);
		}
		updateCollection(collection, items);
	}

	private void updateCollection(Collection collection, JSONArray items) {
		
		int members = 0;
		List nestedCollections = getNestedCollections(collection);
		
		MutableCollection mutable = Catalog.createMutableCollection(collection);
		CatalogAdapter.INSTANCE.setIsVisibleInRoot(mutable, collection.isVisible());
		
		for (int i=0; i < nestedCollections.size(); ++i) {
			CatalogAdapter.INSTANCE.addMember(mutable, nestedCollections.get(i));
			++members;
		}
		
		for (int i=0; i < items.size(); ++i) {
			Object memberKey = items.get(i);
			Object memberUuid = findUuidFor(memberKey);
			if (memberUuid != null) {
				CatalogAdapter.INSTANCE.addMember(mutable, memberUuid);
				++members;
			}
		}
		
		if (members == 0) {
			CatalogAdapter.INSTANCE.setMembers(mutable, new Object[0]);
		}
		
		CatalogTransaction t = Catalog.getBackend().openTransaction();
		t.updateEntry(mutable);
		t.commitSync();
		
		collection.reload();
		Status.set(collection.getName()+ ": "+members +" / "+nestedCollections.size());
	}

	private List getNestedCollections(Collection existing) {
		List colls = new LinkedList();
		
		int count = existing.getSize();
		for (int i=0; i < count; ++i) {
			Entry[] entries = existing.getEntries(COLLECTION_PREDICATE, i, 1);
			if (entries[0] == null || !(entries[0] instanceof Collection)) {
				break;
			}
			colls.add(entries[0].getUUID());
		}
		return colls;
	}

	private Object findUuidFor(Object memberKey) {
		if (memberKey == null || !(memberKey instanceof String)) {
			return null;
		}
		
		String type = null;
		String key = (String) memberKey;
		
		if (!key.startsWith("*")) {
			if (key.startsWith("#")) {
				key = key.substring(1);
			}
			int separator = key.lastIndexOf('^');
			if (separator != -1) {
				type = key.substring(separator+1);
				key = key.substring(0, separator);
			}
		}
		
		Predicate predicate = PredicateFactory.and(new Predicate[]{
				PredicateFactory.equals("cdeKey", key),
				PredicateFactory.startsWith("location", FileSystemImporter.PREFIX),
		});
		if (type != null) {
			predicate = PredicateFactory.and(new Predicate[] {
					predicate,
					PredicateFactory.equals("cdeType", type)
			});
		}
		
		int[] total = new int[1];
		CatalogEntry[] entries =Catalog.find(predicate, false, 0, 1, total, QueryResultDepth.FAST);
		if (total[0] == 1 && entries != null && entries.length == 1) {
			return CatalogAdapter.INSTANCE.getUUID(entries[0]);
		}
		return null;
	}

}
