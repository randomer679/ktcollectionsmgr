package com.mobileread.ixtab.collman.sync;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Status;
import com.mobileread.ixtab.collman.actions.ActionCompletedMessage;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class FileSystemImporter extends Thread {

	public static final String PREFIX = "/mnt/us/documents";
	public static final int PREFIX_LENGTH = PREFIX.length();

	public void run() {
		Status.set("");
		// Fancy UI... a bit hacky, but works :D
		Event.post(Event.SETTINGS_MODE_EPHEMERAL, this, PanelPosition.BOTH);
		Event.post(Event.DISPLAY_COLLECTIONS_ONLY, this, PanelPosition.BOTH);
		Event.post(Event.INVISIBLE_SHOW, this, PanelPosition.BOTH);
		Event.post(Event.FILTER_LOWER_ONLY_OFF, this, PanelPosition.BOTH);
		Event.post(Event.LOWER_PANEL_DISABLE, this, PanelPosition.BOTH);
		Event.post(Event.STARTUP, this, PanelPosition.BOTH);
		Event.post(Event.PATH_ENTRY_CLICKED, Entry.getRoot(), PanelPosition.UPPER);
		Event.post(Event.SETTINGS_MODE_PERSISTENT, this, PanelPosition.BOTH);
		
		sleepABit();
		if (AllCollectionsRemover.run()) {
			createCollections();
		}
	}

	private void createCollections() {
		Predicate predicate = PredicateFactory.and(new Predicate[] {
				PredicateFactory.startsWith("location", PREFIX)
		});
		int chunks = 10;
		//Status.set("Creating collections.");
		Map pathToUuid = new LinkedHashMap();
		for (int offset = 0; offset < Integer.MAX_VALUE; offset += chunks) {
			CatalogEntry[] entries = Catalog.find(predicate, true, offset, chunks, null, QueryResultDepth.FAST);
			if (entries == null || entries.length == 0) {
				break;
			}
			for (int i=0; i < entries.length; ++i) {
				createCollectionsFor(entries[i], pathToUuid);
			}
		}
		ActionCompletedMessage.show();
	}

	private void createCollectionsFor(CatalogEntry entry, Map map) {
		String name = CatalogAdapter.INSTANCE.getTitle(entry);
		File path = new File(CatalogAdapter.INSTANCE.getLocation(entry));
		if (!path.exists()) {
			Status.set(getRelativePath(path.getAbsolutePath())+" not found!");
			sleepABit();
		}
		Status.set(name);
		Collection parent = createRecursively(path.getParentFile(), map);
		if (parent != null) {
			Entry child = Entry.instantiate(entry);
			parent.addEntry(child);
		}
	}

	private void sleepABit() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
		}
	}

	private Collection createRecursively(File path, Map map) {
		if (path == null || path.getAbsolutePath().equals(PREFIX)) {
			return null;
		}
		if (map.containsKey(path.getAbsolutePath())) {
			return (Collection) map.get(path.getAbsolutePath());
		}
		createRecursively(path.getParentFile(), map);
		String name = findUsableName(path.getName(), map);
		Collection parent = (Collection) map.get(path.getParent());
		if (parent == null) {
			parent = Entry.getRoot();
		}
		Collection c = Catalog.createCollection(parent, name);
		map.put(path.getAbsolutePath(), c);
		return c;
	}

	private String findUsableName(String name, Map map) {
		if (!isNameUsed(name, map)) return name;
		for (int suffix=1;;++suffix) {
			String alt = name+"-"+suffix;
			if (!isNameUsed(alt,map)) {
				return alt;
			}
		}
	}

	private boolean isNameUsed(String name, Map map) {
		Iterator it = map.values().iterator();
		while (it.hasNext()) {
			Collection c = (Collection) it.next();
			if (c.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	private String getRelativePath(String path) {
		return path.substring(PREFIX_LENGTH+1);
	}

}
