package com.mobileread.ixtab.collman.sync;

import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.mobileread.ixtab.collman.Status;
import com.mobileread.ixtab.collman.actions.ActionCompletedMessage;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;
import com.mobileread.ixtab.collman.catalog.Item;

public class LegacyExporter extends Thread {

	private static final String JSON_FILE = LegacyImporter.JSON_FILE;
//	private static final String JSON_FILE = "/tmp/collections.json";

	private static final Predicate ITEM_PREDICATE = PredicateFactory.and(new Predicate[] {
			PredicateFactory.notNull("cdeKey"),	
			PredicateFactory.notNull("cdeType"),
			PredicateFactory.startsWith("location", FileSystemImporter.PREFIX),
			PredicateFactory.equals("type", "Entry:Item"),
	});
	
	public void run() {
		Status.set("");
		JSONObject json = new JSONObject();
		boolean ok = true;
		
		Predicate predicate = PredicateFactory.equals("type", "Collection");
		int[] total = new int[1];
		
		for (int offset = 0; offset < Integer.MAX_VALUE; ++offset) {
			CatalogEntry[] entries = Catalog.find(predicate, true, offset, 1, total, QueryResultDepth.FULL);
			if (offset == 0 && total[0] == 0) {
				break;
			}
			if (entries == null || entries.length == 0 || entries[0] == null) {
				break;
			}
			Collection collection = (Collection) Entry.instantiate(entries[0]);
			try {
				exportCollection(collection, json);
			} catch (Throwable t) {
				Status.set("ERROR exporting "+collection.getName() + " "+t.toString());
				ok = false;
				break;
			}
		}
		if (ok) {
			ok = writeToFile(json);
		}
		if (ok) {
			ActionCompletedMessage.show();
		}
	}

	private boolean writeToFile(JSONObject json) {
		boolean ok;
		try {
			FileWriter fw = new FileWriter(JSON_FILE);
			json.writeJSONString(fw);
			fw.close();
			ok = true;
		} catch (Throwable t) {
			Status.set("ERROR writing "+JSON_FILE);
			ok = false;
		}
		return ok;
	}

	private void exportCollection(Collection collection, JSONObject json) throws Exception {
		Status.set(collection.getName());
		
		JSONArray items = new JSONArray();
		int size = 0;
		int max = collection.getSize();
		for (int i=0; i < max; ++i) {
			Entry[] entries = collection.getEntries(ITEM_PREDICATE, i, 1);
			if (entries == null || entries[0] == null) {
				break;
			} else if (entries[0] instanceof Item) {
				items.add(toJsonFormat((Item)entries[0]));
				++size;
			}
		}
		if (size > 0) {
			JSONObject jsonCollection = new JSONObject();
			jsonCollection.put("items", items);
			jsonCollection.put("lastAccess", new Long(CatalogAdapter.INSTANCE.getLastAccessDate(collection.getBackendCollection()).getTime()));
			json.put(collection.getName()+"@en-US", jsonCollection);
		}
	}

	private String toJsonFormat(Item item) throws Exception {
		StringBuffer sb = new StringBuffer("#");
		CatalogItem backend = (CatalogItem) item.getBackend();
		String cdeKey = CatalogAdapter.INSTANCE.getCDEKey(backend);
		// special case
		if (cdeKey.startsWith("*")) {
			return cdeKey;
		}
		sb.append(cdeKey);
		sb.append("^");
		sb.append(CatalogAdapter.INSTANCE.getCDEType(backend));
		
		return sb.toString();
	}

}
