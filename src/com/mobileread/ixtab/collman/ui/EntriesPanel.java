package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Model;
import com.mobileread.ixtab.collman.catalog.Entry;

public class EntriesPanel extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;

	private final EntryPanel[][] childPanelVariants;
	private EntryPanel[] childPanels;
	private final int position;
	private final JPanel innerPanel = new JPanel();

	public EntriesPanel(int position) {
		this.position = position;
		childPanelVariants = createPanelVariants();
		childPanels = childPanelVariants[0];
		setLayout(new BorderLayout());
		add(innerPanel, BorderLayout.CENTER);
		resetInnerPanels();
		innerPanel.addMouseListener(this);
	}

	private EntryPanel[][] createPanelVariants() {
		EntryPanel[] small = new EntryPanel[Model.get(position).getDefaultEntriesPerPage()];
		for (int i=0; i < small.length; ++i) {
			small[i] = new EntryPanel(position);
		}
		if (Model.get(position).getMaxedEntriesPerPage() != small.length) {
			EntryPanel[] large = new EntryPanel[Model.get(position).getMaxedEntriesPerPage()];
			for (int i=0; i < large.length; ++i) {
				if (i < small.length) {
					large[i] = small[i];
				} else {
					large[i] = new EntryPanel(position);
				}
			}
			return new EntryPanel[][] {small, large};
		}
		return new EntryPanel[][] {small};
	}

	private void resetInnerPanels() {
		innerPanel.removeAll();
		innerPanel.setLayout(new GridLayout(childPanels.length, 1, 10, 10));
		for(int i=0; i < childPanels.length; ++i) {
			innerPanel.add(childPanels[i]);
		}
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				Component parent = getRootComponent();
				parent.validate();
				parent.repaint();
			}

		});
	}
	
	protected Component getRootComponent() {
		Component root = this;
		while (root.getParent() != null) {
			root = root.getParent();
		}
		return root;
	}

	public void setEntries(Entry[] entries) {
		boolean changed = choosePanel(entries.length);
		for (int i=0; i < entries.length; ++i) {
			if (i < entries.length) {
				childPanels[i].setEntry(entries[i]);
			} else {
				childPanels[i].setEntry(null);
			}
		}
		if (changed) {
			resetInnerPanels();
		}
		Event.post(Event.VIEW_RESET, this, position);
	}

	private boolean choosePanel(int length) {
		if (childPanels.length == length) {
			return false;
		}
		if (length < childPanels.length) {
			for (int i=length; i < childPanels.length; ++i) {
				childPanels[i].setEntry(null);
			}
		}
		childPanels = childPanelVariants[length < childPanels.length ? 0 : 1];
		return true;
	}

	public void mouseClicked(MouseEvent e) {
	}

	Point mouseStart = null;
	long mouseStartTs = 0;
	public void mousePressed(MouseEvent e) {
		if (e == null) {
			return;
		}
		mouseStart = e.getPoint();
		mouseStartTs = System.currentTimeMillis();
	}

	public void mouseReleased(MouseEvent e) {
		if (e == null) {
			return;
		}
		Point mouseEnd = e.getPoint();
		if (mouseStart != null && mouseEnd != null) {
			int xdelta = mouseEnd.x - mouseStart.x;
			if (xdelta > 25) {
				Event.post(Event.COMMAND_LEFT, this, position);
			} else if (xdelta < -25) {
				Event.post(Event.COMMAND_RIGHT, this, position);
			} else {
				// the framework causes some weird clicks within swipes, at times.
				// this seems to remedy it.
				long tdelta = System.currentTimeMillis() - mouseStartTs;
				if (tdelta > 50) {
					Component c = innerPanel.getComponentAt(mouseEnd);
					if (c instanceof EntryPanel) {
						EntryPanel panel = (EntryPanel) c;
						if (tdelta > 1000) {
							panel.doLongClick();
						} else {
							panel.doClick();
						}
					}
				}
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
