package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class EntryPanel extends JPanel implements ItemListener, ActionListener {
	private static final long serialVersionUID = 1L;

	private static Font collectionFont = null;
	private static Font entryFont = null;

	private final JLabel name = new JLabel();
	private final JLabel count = new JLabel();
	private final JCheckBox checkbox = new JCheckBox();
	private final int position;
	private Entry entry;
	
	// that's exactly between Color.GRAY and Color.DARK_GRAY. The former
	// is just a bit too light on the PW, while the latter is just a bit
	// too dark on the Touch.
	private static final Color COLOR_HIDDEN = new Color(96, 96, 96);

	public EntryPanel(final int position) {
		prepareFonts();
		Event.addListener(this);
		this.position = position;
		setLayout(new BorderLayout(5, 5));

		// This is a hack to ensure that checkboxes are neatly aligned with the "toggle all" box
		checkbox.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0,
				Color.WHITE));
		add(checkbox, BorderLayout.WEST);
		add(name, BorderLayout.CENTER);
		add(count, BorderLayout.EAST);

		checkbox.addItemListener(this);
	}

	private void prepareFonts() {
		if (collectionFont == null) {
			collectionFont = count.getFont();
			entryFont = new Font("Serif", Font.PLAIN, count.getFont().getSize());
		}
	}

	void setEntry(Entry entry) {
		checkbox.setSelected(false);

		if (entry == null) {
			name.setForeground(Color.WHITE);
			name.setText("DUMMY");
			count.setForeground(Color.WHITE);
			count.setText("DMY");
			checkbox.setVisible(false);
		} else {
			checkbox.setVisible(true);
			String desc = entry.toString();
			name.setFont(entry instanceof Collection ? collectionFont
					: entryFont);
			name.setText(desc);
			count.setFont(entry instanceof Collection ? collectionFont
					: entryFont);
			count.setText("( " + entry.getCollectionsCount() + " )  ");
			Color color = entry.isVisible() ? Color.BLACK : COLOR_HIDDEN;
			name.setForeground(color);
			count.setForeground(color);
		}
		this.entry = entry;
	}

	public void itemStateChanged(ItemEvent e) {
		int type = e.getStateChange() == ItemEvent.SELECTED ? Event.VIEW_ENTRY_SELECTED
				: Event.VIEW_ENTRY_UNSELECTED;
		if (entry != null) {
			Event.post(type, entry, position);
		}
	}

	public void actionPerformed(final ActionEvent e) {
		if (e.getID() == Event.TOGGLE_ALL_SELECTIONS && ((Event) e).appliesTo(position)) {
			if (entry != null) {
				checkbox.doClick();
			}
			return;
		}

		if (e.getID() == Event.ENTRY_CHANGED
				&& Entry.equals(e.getSource(), entry)) {
			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					setEntry((Entry) e.getSource());
				}
			});
			Event.post(Event.VIEW_CHANGED, e.getSource(), position);
		}
	}

	public void doClick() {
		if (entry != null && entry instanceof Collection) {
			Event.post(Event.VIEW_ENTRY_CLICKED, entry, position);
		}
	}

	public void doLongClick() {
		if (entry != null && entry.getCollectionsCount() > 0) {
			Collection[] parents = entry.getCollections();
			if (parents.length > 0) {
				StringBuffer s = new StringBuffer();
				for (int i = 0; i < parents.length; ++i) {
					// s.append(parents[i].toString());
					s.append(parents[i].getName());
					s.append("\n");
				}
				KOptionPane.showMessageDialog(this, s.toString(),
						entry.getName());
			}
		}
	}

}
