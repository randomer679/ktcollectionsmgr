package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Model;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;

public class BrowsePanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private final JButton previousButton = new JButton("<");
	private final JButton nextButton = new JButton(">");
	private final EntriesPanel panel; 
	private final DefaultComboBoxModel path = new DefaultComboBoxModel();
	private final PathPanel pathPanel;
	private final int position;
	private final Model model;
	
	private Collection collection;
	
	public BrowsePanel(int position) {
		this.position = position;
		this.model = Model.get(position);
		panel = new EntriesPanel(position);
		pathPanel = new PathPanel(path, position);
		Event.addListener(this);
		setLayout(new BorderLayout(5,5));
		setBorder(Borders.GRAY2WHITE5);
		add(panel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 5, 0));
		buttonsPanel.add(previousButton);
		buttonsPanel.add(nextButton);
		addAction(previousButton, Event.COMMAND_LEFT);
		addAction(nextButton, Event.COMMAND_RIGHT);
		
		JPanel northPanel = new JPanel(new BorderLayout(5,5));
		northPanel.add(pathPanel, BorderLayout.CENTER);
		northPanel.add(buttonsPanel, BorderLayout.EAST);
		
		add(northPanel, BorderLayout.NORTH);
		setCollection(Model.get(position).getCurrentCollection());
	}
	
	private void addAction(JButton button, final int event) {
		button.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				Event.post(event, BrowsePanel.this, position);
			}});
	}

	private void setCollection(final Collection collection) {
		this.collection = collection;
		
		final Entry[] entries = collection.getEntries(model.getFilter(), model.getOffset(), model.getEntriesPerPage());
		int existingIndex = findPositionOfCurrentCollection();
		if (existingIndex == -1) {
			path.addElement(collection);
		} else {
			stripToSize(existingIndex+1);
		}
		Event.post(Event.RELOADED, collection, position);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				panel.setEntries(entries);
				path.setSelectedItem(path.getElementAt(path.getSize()-1));
			}
		});
	}

	private int findPositionOfCurrentCollection() {
		for (int i=path.getSize()-1; i>= 0; --i) {
			if (Entry.equals(collection, path.getElementAt(i))) {
				return i;
			}
		}
		return -1;
	}

	private void stripToSize(int size) {
		while (path.getSize() > size) {
			path.removeElementAt(path.getSize()-1);
		}
	}

	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (!e.appliesTo(position)) {
			return;
		}
		if (e.getID() == Event.LOWER_PANEL_DISABLE || e.getID() == Event.LOWER_PANEL_ENABLE) {
			if (position == PanelPosition.LOWER) {
				if (e.getID() == Event.LOWER_PANEL_DISABLE) {
					panel.setEntries(new Entry[model.getEntriesPerPage()]);
					setVisible(false);
				} else {
					setCollection(model.getCurrentCollection());
					setVisible(true);
				}
			}
			return;
		}
		if (e.getID() == Event.RELOAD) {
			setCollection(model.getCurrentCollection());
		}
		if (e.getID() == Event.ENABLE_LEFT) {
			previousButton.setEnabled(true);
		}
		if (e.getID() == Event.DISABLE_LEFT) {
			previousButton.setEnabled(false);
		}
		if (e.getID() == Event.ENABLE_RIGHT) {
			nextButton.setEnabled(true);
		}
		if (e.getID() == Event.DISABLE_RIGHT) {
			nextButton.setEnabled(false);
		}
	}
}
