package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Event;

public class PathPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private final JComboBox path;
	private final JButton upButton = new JButton("^");
	private final JCheckBox toggleAll = new JCheckBox();

	public PathPanel(ComboBoxModel model, final int position) {
		upButton.setEnabled(false);
		path = new JComboBox(model);
		Font f = path.getFont();
		path.setFont(new Font(f.getName(), f.getStyle(), (int)(f.getSize() * 1.2)));
		setLayout(new BorderLayout(5,5));
		add(path, BorderLayout.CENTER);
		
		JPanel westPanel = new JPanel(new GridLayout(1, 2, 5, 5));
		westPanel.add(toggleAll);
		toggleAll.setSelected(true);
//		toggleAll.setEnabled(false);
		toggleAll.setForeground(new Color(96,96,96));
//		toggleAll.setBackground(Color.BLACK);
		toggleAll.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				toggleAll.setSelected(true);
				Event.post(Event.TOGGLE_ALL_SELECTIONS, this, position);
			}
			
		});
		westPanel.add(upButton);
		add(westPanel, BorderLayout.WEST);
		
		path.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object source = path.getSelectedItem();
				if (source != null) {
					Event.post(Event.PATH_ENTRY_CLICKED, source, position);
					upButton.setEnabled(path.getSelectedIndex() > 0);
				}
			}
		});
		
		upButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				path.setSelectedIndex(Math.max(0, path.getSelectedIndex()-1));
			}
		});
		Event.addListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		final Event event = (Event)e;
		if (event.getID() == Event.RELOAD) {
			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					path.repaint();
				}
				
			});
		}
	}
}
