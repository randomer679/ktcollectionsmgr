package com.mobileread.ixtab.collman;

import ixtab.jailbreak.Jailbreak;
import ixtab.jailbreak.SuicidalKindlet;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.security.AllPermission;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;

public class CollectionManagerKindlet extends SuicidalKindlet {

	private KindletContext context;
	private CollectionManager ui;
	private JLabel centerLabel;
	
	protected Jailbreak instantiateJailbreak() {
		return new LocalJailbreak();
	}


	public void onCreate(KindletContext kindletContext) {
		this.context = kindletContext;
		//final Container root = context.getRootContainer();
		String startupMessage = "www.ixtab.tk";
		this.centerLabel = new JLabel(startupMessage);
		initUi();
	}
	
	

	protected void onStart() {
		if (ui == null) {
			new Thread() {
				public void run() {
					try {
						while (!(context.getRootContainer().isValid() && context.getRootContainer().isVisible())) {
							Thread.sleep(100);
						}
						Thread.sleep(2500);
					} catch (Exception e) {};
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							initUiAfterSplash();
						}
					});
				}
			}.start();
		}
	}
	
	protected void onStop() {
		if (ui != null) {
			ui.onStop();
		}
	}


	private void initUiAfterSplash() {
		if (jailbreak.isAvailable()) {
			if (((LocalJailbreak)jailbreak).requestPermissions()) {
				context.getRootContainer().removeAll();
				ui = AdapterConfiguration.INSTANCE.getCollectionManager(context);
				ui.init();
			} else {
				String title = "Kindlet Jailbreak Failed";
				setCentralMessage(title);
				String error = "The Kindlet Jailbreak failed to obtain all required permissions. Please report this error.";
				KOptionPane.showMessageDialog(context.getRootContainer(), error, title);
			}
		} else {
			String title = "Kindlet Jailbreak Required";
			String message = "This application requires the Kindlet Jailbreak to be installed. This is an additional jailbreak that must be installed on top of the Device Jailbreak, in order to allow Kindlets to get the required permissions. Please install the Kindlet Jailbreak before using this application.";
			setCentralMessage(title);
			KOptionPane.showMessageDialog(context.getRootContainer(), message, title);
		}
	}

	private void setCentralMessage(String centered) {
		centerLabel.setText(centered);
		context.getRootContainer().validate();
	}

	
	private void initUi() {
		Container pane = context.getRootContainer();
		pane.removeAll();
		centerLabel.setFont(new Font(centerLabel.getFont().getName(), Font.BOLD, centerLabel.getFont().getSize() + 6));
		
		pane.setLayout(new GridBagLayout());
		
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.fill |= GridBagConstraints.VERTICAL;
        gbc.weightx  = 1.0;
        gbc.weighty  = 1.0;
        
		pane.add(centerLabel, gbc);
	}

	private static class LocalJailbreak extends Jailbreak {

		public boolean requestPermissions() {
			boolean ok  = getContext().requestPermission(new AllPermission());
			return ok;
		}

	}

}