package com.mobileread.ixtab.collman.adapters.fw534;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionManager;

public class CollectionManager534 extends CollectionManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager534(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
			return null;
	}

	protected void modifyToolbar() {
	}

	public void onStop() {
	}

	protected boolean isSearchSupported() {
		return false;
	}
}
