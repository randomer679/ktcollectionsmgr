package com.mobileread.ixtab.collman.adapters.fw512;

import com.amazon.ebook.util.lang.UUID;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction512 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction512(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.deleteEntry((UUID) uuid);
	}

	public boolean commitSync() {
		return delegate.commitSync().isSuccessful();
	}

	public void addEntry(MutableEntry c) {
		delegate.addEntry(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.updateEntry(entry);
	}

}
