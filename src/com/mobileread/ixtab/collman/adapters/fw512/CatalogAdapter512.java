package com.mobileread.ixtab.collman.adapters.fw512;

import java.util.Date;

import com.amazon.ebook.util.lang.UUID;
import com.amazon.ebook.util.text.LString;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.Credit;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter512 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		UUID[] ids = asUUIDArray(uuids);
		mutable.setMembers(ids);
	}

	protected static UUID[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		UUID[] out = new UUID[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (UUID) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.addMember((UUID) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		mutable.setIsVisibleInHome(visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		mutable.setTitles(new LString[] {new LString(title)});
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.getUUID();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.getLocation();
	}

	public String getTitle(CatalogEntry entry) {
		LString[] titles = entry.getTitles();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.getCDEKey();
	}

	public String getCDEType(CatalogItem item) {
		return item.getCDEType();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.getLastAccessDate();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.isVisibleInHome();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.setIsVisibleInHome(visible);
	}

	public int getCollectionCount(CatalogEntry entry) {
		return entry.getCollectionCount();
	}

	public Object[] getCollections(CatalogEntry entry) {
		return entry.getCollections();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		Credit[] credits = entry.getCredits();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.getMembers();
	}

	public int getMemberCount(CatalogEntryCollection collection) {
		return collection.getMemberCount();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.setTitles(new LString[] {new LString(title)});
	}

}
