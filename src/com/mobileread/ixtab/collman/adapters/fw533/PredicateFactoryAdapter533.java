package com.mobileread.ixtab.collman.adapters.fw533;

import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.g;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter533 implements PredicateFactoryAdapter {
	
	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private g[] explode(
			Predicate[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(b.OcB(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(b.YdB(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(b.KbB((g) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(b.EdB(key));
	}

	public Predicate isTrue(String what) {
		return wrap(b.ydB(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(b.hvA(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(b.RbB(key, CatalogAdapter533.asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(b.TCB(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(b.uAB(key, value));
	}

}
