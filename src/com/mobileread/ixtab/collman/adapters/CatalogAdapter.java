package com.mobileread.ixtab.collman.adapters;

import java.util.Date;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;

public abstract class CatalogAdapter {

	public static final CatalogAdapter INSTANCE = AdapterConfiguration.INSTANCE.getCatalogAdapter();

	public abstract Object getUUID(CatalogEntry entry);
	public abstract String getLocation(CatalogEntry entry);
	public abstract String getTitle(CatalogEntry entry);
	public abstract String getCDEKey(CatalogItem item);
	public abstract String getCDEType(CatalogItem item);
	public abstract Date getLastAccessDate(CatalogEntryCollection collection);
	public abstract boolean isVisibleInHome(CatalogEntry entry);
	public abstract void setIsVisibleInHome(MutableEntry mutable, boolean visible);
	public abstract int getCollectionCount(CatalogEntry entry);
	public abstract Object[] getCollections(CatalogEntry entry);
	public abstract String getFirstCreditAsJSON(CatalogEntry entry);
	public abstract Object[] getMembers(CatalogEntryCollection collection);
	public abstract int getMemberCount(CatalogEntryCollection collection);
	public abstract void setTitle(MutableEntry mutable, String newTitle);
	public abstract void setIsVisibleInRoot(MutableCollection mutable, boolean visible);
	public abstract void setTitle(MutableCollection mutable, String title);
	public abstract void setMembers(MutableCollection mutable, Object[] uuids);
	public abstract void addMember(MutableCollection mutable, Object uuid);
}
