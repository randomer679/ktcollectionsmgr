package com.mobileread.ixtab.collman.adapters.fw531;

import java.util.Map;
import com.amazon.ebook.util.a.g;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogService.QueryResults;
import com.amazon.kindle.content.catalog.a;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;

public class CatalogService531 extends CatalogService {

	private static a[] collation = null;
	
	private static synchronized a[] getCollation() {
		if (collation == null) {
			String name = getCollationName();
			if (name == null) {
				return new a[] { new a(COLLATION_DISPLAY) };
			}
			collation = new a[] {new a(name)};
		}
		return collation;
	}

	private final com.amazon.kindle.content.catalog.CatalogService delegate = (com.amazon.kindle.content.catalog.CatalogService) Framework
			.getService(com.amazon.kindle.content.catalog.CatalogService.class);

	public MutableEntry createMutableEntry(Object uuid) {
		return delegate.zH((g) uuid);
	}

	public MutableCollection createMutableCollection(Object uuid) {
		return delegate.Li((g) uuid);
	}

	public Object createNewUUID() {
		return new g();
	}

	public CatalogTransaction openTransaction() {
		return new CatalogTransaction531(delegate.Zh());
	}

	public CatalogEntry[] find(Predicate predicate, int offset, int maxResults,
			final int[] totalCountPointer, QueryResultDepth depth) {
		// easy way to simulate a pointer using an array
		final CatalogEntry[][] raw = new CatalogEntry[][] { new CatalogEntry[0] };
		synchronized (raw) {
			if (delegate
					.tI((com.amazon.kindle.content.catalog.g) predicate.delegate,
							getCollation(), maxResults, offset, new QueryResults() {

								public void NC(boolean flag, int i,
										int j, CatalogEntry[] acatalogentry,
										Map map) {
									if (totalCountPointer != null) {
										totalCountPointer[0] = i;
									}
									synchronized (raw) {
										raw[0] = acatalogentry;
										raw.notify();
									}
								}
							}, convert(depth))) {
				try {
					raw.wait();
				} catch (InterruptedException e) {
				}
				return raw[0];
			} else {
				return new CatalogEntry[0];
			}
		}
	}

	private com.amazon.kindle.content.catalog.M convert(
			QueryResultDepth depth) {
		return depth == QueryResultDepth.FAST ? com.amazon.kindle.content.catalog.M.e
				: com.amazon.kindle.content.catalog.M.B;
	}

}
