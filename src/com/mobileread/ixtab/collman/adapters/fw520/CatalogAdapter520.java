package com.mobileread.ixtab.collman.adapters.fw520;

import java.util.Date;

import com.amazon.ebook.util.a.g;
import com.amazon.ebook.util.text.m;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter520 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		g[] ids = asUUIDArray(uuids);
		mutable.kg(ids);
	}

	protected static g[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.zf((g) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		mutable.cf(visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		mutable.hF(new m[] {new m(title)});
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.be();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.yF();
	}

	public String getTitle(CatalogEntry entry) {
		m[] titles = entry.Wg();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.zD();
	}

	public String getCDEType(CatalogItem item) {
		return item.Be();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.MG();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.EF();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.cf(visible);
	}

	public int getCollectionCount(CatalogEntry entry) {
		return entry.jg();
	}

	public Object[] getCollections(CatalogEntry entry) {
		return entry.Cg();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.aE();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.Pe();
	}

	public int getMemberCount(CatalogEntryCollection collection) {
		return collection.ne();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.hF(new m[] {new m(title)});
	}

}
