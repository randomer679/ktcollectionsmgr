package com.mobileread.ixtab.collman.adapters.fw520;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration520 extends AdapterConfiguration {

	public AdapterConfiguration520() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter520();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter520();
	}

	public CatalogService getCatalogService() {
		return new CatalogService520();
	}

	public CollectionManager getCollectionManager(KindletContext context) {
		return new CollectionManager520(context);
	}

}
