package com.mobileread.ixtab.collman.adapters.fw530;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration530 extends AdapterConfiguration {

	public AdapterConfiguration530() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter530();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter530();
	}

	public CatalogService getCatalogService() {
		return new CatalogService530();
	}

	public CollectionManager getCollectionManager(KindletContext context) {
		return new CollectionManager530(context);
	}

}
