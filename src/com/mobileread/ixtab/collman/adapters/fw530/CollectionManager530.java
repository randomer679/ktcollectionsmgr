package com.mobileread.ixtab.collman.adapters.fw530;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionManager;

public class CollectionManager530 extends CollectionManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager530(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
			return null;
	}

	protected void modifyToolbar() {
	}

	public void onStop() {
	}

	protected boolean isSearchSupported() {
		return false;
	}
}
