package com.mobileread.ixtab.collman.adapters;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionManager;

public abstract class AdapterConfiguration {
	public static final AdapterConfiguration INSTANCE;
	
	static {
		String className = "com.mobileread.ixtab.collman.adapters.fw512.AdapterConfiguration512";
		try {
			// this class is present in the 5.1.2 Firmware, but not in 5.2.0.
			Class.forName("com.amazon.ebook.util.lang.UUID");
		} catch (Throwable t) {
			className = "com.mobileread.ixtab.collman.adapters.fw520.AdapterConfiguration520";
			try {
				// this class is present in 5.3.0 and later, but not in 5.2.0
				Class.forName("com.amazon.kindle.custom_screensavers.a.b");
				className = "com.mobileread.ixtab.collman.adapters.fw530.AdapterConfiguration530";
				try {
					// present in 5.3.1, but not 5.3.0
					Class.forName("com.amazon.kindle.home.d.G");
					className = "com.mobileread.ixtab.collman.adapters.fw531.AdapterConfiguration531";
					try {
						// present in 5.3.2 (Touch), but not 5.3.1
						Class.forName("com.amazon.kindle.booklet.ad.resources.AdResources_sq");
						className = "com.mobileread.ixtab.collman.adapters.fw532.AdapterConfiguration532";
					} catch (Throwable t4) {
						// 5.3.3 (PW).
						Class.forName("com.amazon.ebook.booklet.topazreader.impl.A");
						className = "com.mobileread.ixtab.collman.adapters.fw533.AdapterConfiguration533";
						try {
							Class.forName("com.amazon.ebook.booklet.reader.sdk.internal.SimpleBookView");
							className = "com.mobileread.ixtab.collman.adapters.fw534.AdapterConfiguration534";
						} catch (Throwable t5) {}
					}
				} catch (Throwable t3 ){
				}
			} catch (Throwable t2) {}
		}
		
		try {
			Class clazz = Class.forName(className);
			INSTANCE = (AdapterConfiguration) clazz.newInstance();
		} catch (Throwable t) {
			throw new RuntimeException("FATAL ERROR: Unable to instantiate "+className, t);
		}
	}

	public abstract CollectionManager getCollectionManager(KindletContext context);
	public abstract PredicateFactoryAdapter getPredicateFactoryAdapter();
	public abstract CatalogService getCatalogService();
	public abstract CatalogAdapter getCatalogAdapter();
	
}
