package com.mobileread.ixtab.collman.adapters;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.I18n;

public abstract class CatalogService {
	
	protected static final String COLLATION_DISPLAY = "titles[0].display";
	protected static final String COLLATION_COLLATE = "titles[0].collation";

	protected static String getCollationName() {
		try {
			return "true".equalsIgnoreCase(I18n.get().i18n(I18n.ALT_COLLATION_KEY, I18n.ALT_COLLATION_VALUE)) ? COLLATION_COLLATE: COLLATION_DISPLAY;
		} catch (Throwable t) {
			return null;
		}
	}
	
	public static final CatalogService INSTANCE = AdapterConfiguration.INSTANCE.getCatalogService();
	
	public static class QueryResultDepth {
		private final String type;
		public static final QueryResultDepth FAST = new QueryResultDepth("FAST");
		public static final QueryResultDepth FULL = new QueryResultDepth("FULL");
		
		private QueryResultDepth(String type) {
			this.type = type;
		}
		
		public String toString() {
			return type;
		}

		
	}

	public abstract MutableEntry createMutableEntry(Object uuid);

	public abstract MutableCollection createMutableCollection(Object uuid);

	public abstract Object createNewUUID();

	public abstract CatalogTransaction openTransaction();

	public abstract CatalogEntry[] find(Predicate predicate, int offset, int maxResults,
			int[] totalCountPointer, QueryResultDepth depth); {
	}
}
