package com.mobileread.ixtab.collman;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;


public class I18n {
	
	private static final I18n instance = new I18n(getLocalizationURL());
	
	public static final String DISPLAY_COLLECTIONS_KEY = "display.collectionsonly";
	public static final String DISPLAY_COLLECTIONS_VALUE = "Display collections only";

	public static final String FILTER_LOWERONLY_KEY = "filter.loweronly";
	public static final String FILTER_LOWERONLY_VALUE = "Limit filters to the lower pane";
	
	public static final String INVISIBLE_SHOW_KEY = "invisible.show";
	public static final String INVISIBLE_SHOW_VALUE = "Show invisible items in Home";
	
	public static final String IMPORT_CALIBRE_KEY = "import.calibre";
	public static final String IMPORT_CALIBRE_VALUE = "Import collections from Calibre";
	public static final String EXPORT_CALIBRE_KEY = "export.calibre";
	public static final String EXPORT_CALIBRE_VALUE = "Export collections to Calibre";
	
	public static final String UPDATE_DIR_MENU_KEY = "update.dir.menu";
	public static final String UPDATE_DIR_MENU_VALUE = "Create collections from directories";
	public static final String UPDATE_DIR_MESSAGE_KEY = "update.dir.msg";
	public static final String UPDATE_DIR_MESSAGE_VALUE = "Attention: this is a potentially dangerous action. It will DELETE all collections on your device, and then create collections according to the directory structure of the documents/ folder.\n\nAre you sure you want to continue?";
	public static final String UPDATE_DIR_TITLE_KEY = "update.dir.title";
	public static final String UPDATE_DIR_TITLE_VALUE = "Import collections";
	
	public static final String DELETE_ALL_MENU_KEY = "delete.all.menu";
	public static final String DELETE_ALL_MENU_VALUE = "Delete all collections";
	public static final String DELETE_ALL_MESSAGE_KEY = "delete.all.msg";
	public static final String DELETE_ALL_MESSAGE_VALUE = "Are you sure you want to continue?";
	
	public static final String ACTION_DONE_KEY = "action.done";
	public static final String ACTION_DONE_VALUE = "Action completed successfully.";
	
	public static final String SHOW_UNFILED_ONLY_KEY = "show.unfiled.only";
	public static final String SHOW_UNFILED_ONLY_VALUE = "Upper pane: show unfiled items only";
	
	public static final String ALT_COLLATION_KEY = "alternative.collation";
	public static final String ALT_COLLATION_VALUE = "false";
	
	private static URL getLocalizationURL() {
		try {
			File f = new File("/mnt/us/documents/CollectionsManager-localization.txt");
			if (f.exists() && f.isFile() && f.canRead()) {
				return f.toURL();
			}
		} catch (Throwable t) {
		}
		return null;
	}
	
	public static I18n get() {
		return instance;
	}
	
	private final String[] locales;
	private final Map[] translations;
	
	public I18n() {
		this((InputStream) null, null);
	}
	
	public I18n(URL url) {
		this(url, null);
	}
	
	public I18n(URL url, Locale locale) {
		this (safeStream(url), locale);
	}
	
	public I18n(InputStream stream) {
		this(stream, null);
	}
	
	public I18n(InputStream stream, Locale locale) {
		locales = determineLocalesSearchOrder(locale != null ? locale : Locale.getDefault());
		translations = initializeTranslations(locales.length);
		if (stream != null) {
			fillTranslations(stream);
		}
	}

	private static InputStream safeStream(URL url) {
		if (url == null) return null;
		try {
			return url.openStream();
		} catch (IOException e) {
			return null;
		}
	}

	private String[] determineLocalesSearchOrder(Locale current) {
		List list = new ArrayList(3);
		list.add(current);
		Locale last = current;
		current = new Locale(last.getLanguage());
		if (!current.equals(last)) {
			list.add(current);
		}
		if (!current.equals(Locale.ENGLISH)) {
			list.add(Locale.ENGLISH);
		}
		
		String[] result = new String[list.size()];
		for (int i=0; i < result.length; ++i) {
			result[i] = ((Locale)list.get(i)).toString();
		}
		return result;
	}

	private Map[] initializeTranslations(int count) {
		Map[] map = new Map[count];
		for (int i=0; i < count; ++i) {
			map[i] = new TreeMap();
		}
		return map;
	}
	
	private void fillTranslations(InputStream stream) {
		BufferedReader r = null;
		try {
			r= new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			Map translation = null;
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				if (isIgnorable(line)) continue;
				String localeCode = interpretAsSection(line);
				if (localeCode != null) {
					translation = lookupTranslationFor(localeCode);
					continue;
				}
				insertIfValid(line, translation);
			}
		} catch (IOException e) {
		} finally {
			if (r != null) {
				try {
					r.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private boolean isIgnorable(String line) {
		String trimmed = line.trim();
		return trimmed.length() == 0 || trimmed.startsWith("#");
	}

	private String interpretAsSection(String line) {
		// allow trailing spaces, but not leading ones
		String trimmed = line.trim();
		if (line.startsWith("[") && trimmed.endsWith("]")) {
			return trimmed.substring(1, trimmed.length()-1);
		}
		return null;
	}

	private Map lookupTranslationFor(String localeCode) {
		for (int i=0; i < locales.length; ++i) {
			if (locales[i].equals(localeCode)) {
				return translations[i];
			}
		}
		return null;
	}

	private void insertIfValid(String line, Map translation) {
		if (translation == null) return;
		int delim = line.indexOf('=');
		if (delim == -1) return;
		String key = line.substring(0, delim).trim();
		String value = line.substring(delim+1).trim();
		translation.put(key, value);
	}

	public String[] getOrderedLocales() {
		String[] copy = new String[locales.length];
		System.arraycopy(locales, 0, copy, 0, locales.length);
		return copy;
	}
	
	public String i18n(String key, String fallback) {
		key = key.trim();
		
		for (int i=0; i < translations.length; ++i) {
			Object match = translations[i].get(key);
			if (match != null) {
				return (String) match;
			}
		}
		return fallback;
	}

}
