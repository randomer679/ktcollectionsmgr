PRODUCT=CollectionsManager
VERSION=2.3.6

rm *.zip

zip -9 -r ${PRODUCT}_${VERSION}.zip LICENSE.txt CREDITS.txt *.azw2 *-help.png

